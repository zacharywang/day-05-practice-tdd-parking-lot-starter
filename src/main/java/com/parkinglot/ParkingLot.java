package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private final Map<ParkingTicket, Car> parkingTicketCarMap = new HashMap<>();
    private static final int carCapacity = 10;
    private int spareParkingSpaces = carCapacity;

    public ParkingTicket park(Car car) {
        spareParkingSpaces--;
        if (spareParkingSpaces < 0) {
            throw new UnrecognizedParkingTicketException("No available position.");
        }
        ParkingTicket parkingTicket = new ParkingTicket();
        parkingTicketCarMap.put(parkingTicket, car);
        return parkingTicket;
    }

    public Car fetch(ParkingTicket parkingTicket) {
        Car car = parkingTicketCarMap.get(parkingTicket);
        if (car == null) {
            throw new UnrecognizedParkingTicketException("Unrecognized parking ticket");
        }
        parkingTicketCarMap.remove(parkingTicket);
        spareParkingSpaces++;
        return car;
    }

    public boolean isFull() {
        return parkingTicketCarMap.size() >= carCapacity;
    }

    public int countCurrentCar() {
        return spareParkingSpaces;
    }
}
