package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class ParkingBoyTest {
    @Test
    void should_return_a_ticket_when_parkingboy_park_the_car_given_a_car(){
        // Given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        Car car = new Car();

        // When
        ParkingTicket ticket = parkingBoy.park(car);

        // Then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_car_when_parkingboy_park_the_car_given_a_ticket(){
        // Given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        Car car = new Car();
        ParkingTicket ticket = parkingBoy.park(car);
        // When
        Car fetchedCar = parkingBoy.fetch(ticket);

        // Then
        Assertions.assertSame(car,fetchedCar);
    }
    @Test
    void should_return_correct_two_cars_when_parkingboy_park_the_car_given_two_tickets(){
        // Given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket ticket1 = parkingBoy.park(car1);
        ParkingTicket ticket2 = parkingBoy.park(car2);

        // When
        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        Car fetchedCar2 = parkingBoy.fetch(ticket2);


        // Then
        Assertions.assertSame(car1,fetchedCar1);
        Assertions.assertSame(car2,fetchedCar2);

    }

    @Test
    void should_return_error_message_when_parkingboy_park_the_car_given_wrong_tickets(){
        // Given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        Car car = new Car();
        ParkingTicket ticket = parkingBoy.park(car);
        ParkingTicket wrongTicket = new ParkingTicket();

        // When


        // Then
        Assertions.assertThrows(UnrecognizedParkingTicketException.class,()->parkingBoy.fetch(wrongTicket));

    }

    @Test
    void should_return_error_message_when_parkingboy_park_the_car_given_has_already_used_tickets(){
        // Given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        Car car = new Car();
        ParkingTicket ticket = parkingBoy.park(car);

        // When
        Car fetchedCar = parkingBoy.fetch(ticket);

        // Then
        Assertions.assertThrows(UnrecognizedParkingTicketException.class,()->parkingBoy.fetch(ticket));

    }

    @Test
    void should_return_error_message_when_parkingboy_park_the_car_given_parkinglog_without_any_position(){
        // Given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        for (int i = 0; i < 10; i++) {
            Car car = new Car();
            ParkingTicket ticket = parkingBoy.park(car);
        }
        Car car = new Car();
        // When

        // Then
        Assertions.assertThrows(UnrecognizedParkingTicketException.class,()->parkingBoy.park(car));

    }

    @Test
    void should_return_the_2th_parkinglot_ticket_when_boy_park_over_first_parkinglot_capacity_given_two_parkinglot(){
        // Given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = Arrays.asList(parkingLot1, parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        // When
        for (int i = 0; i < 10; i++) {
            parkingBoy.park(new Car());
        }
        // Then
        int parkingLot1CountCurrentCar = parkingLot1.countCurrentCar();
        int parkingLot2CountCurrentCar = parkingLot2.countCurrentCar();
        parkingBoy.park(new Car());
        int parkingLot2CountCurrentCar2 = parkingLot2.countCurrentCar();
        Assertions.assertSame(0,parkingLot1CountCurrentCar);
        Assertions.assertSame(10,parkingLot2CountCurrentCar);
        Assertions.assertSame(9,parkingLot2CountCurrentCar2);

    }

    @Test
    void should_return_the_error_message_when_boy_park_over_second_parkinglot_capacity_given_two_parkinglot(){
        // Given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = Arrays.asList(parkingLot1, parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        // When
        for (int i = 0; i < 20; i++) {
            parkingBoy.park(new Car());
        }
        // Then
        int parkingLot1CountCurrentCar = parkingLot1.countCurrentCar();
        int parkingLot2CountCurrentCar = parkingLot2.countCurrentCar();

        Assertions.assertSame(0,parkingLot1CountCurrentCar);
        Assertions.assertSame(0,parkingLot2CountCurrentCar);
        Assertions.assertThrows(UnrecognizedParkingTicketException.class,() -> parkingBoy.park(new Car()));

    }

    @Test
    void should_return_the_correct_car_when_boy_fetch_car_given_two_parkinglot(){
        // Given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = Arrays.asList(parkingLot1, parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        // When
        for (int i = 0; i < 19; i++) {
            parkingBoy.park(new Car());
        }
        Car car = new Car();
        ParkingTicket parkingTicket = parkingBoy.park(car);
        // Then
        int parkingLot1CountCurrentCar = parkingLot1.countCurrentCar();
        int parkingLot2CountCurrentCar = parkingLot2.countCurrentCar();

        Assertions.assertSame(0,parkingLot1CountCurrentCar);
        Assertions.assertSame(0,parkingLot2CountCurrentCar);

        Car fetchedCar = parkingBoy.fetch(parkingTicket);
        Assertions.assertSame(car,fetchedCar);

    }


}
