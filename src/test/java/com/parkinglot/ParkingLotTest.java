package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_a_parking_lot_ticket_when_park_the_car_given_a_car() {
        // Given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();

        // When
        ParkingTicket parkingTicket = parkingLot.park(car);

        // Then
        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    void should_return_a_car_when_fetch_the_car_given_a_parking_lot_ticket() {
        // Given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);

        // When
        Car fetchedCar = parkingLot.fetch(parkingTicket);

        // Then
        Assertions.assertSame(car,fetchedCar);
    }

    @Test
    void should_return_correct_two_car_when_fetch_the_car_given_two_parking_lot_ticket() {
        // Given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);

        // When
        Car fetchedCar1 = parkingLot.fetch(parkingTicket1);
        Car fetchedCar2 = parkingLot.fetch(parkingTicket2);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertSame(car1, fetchedCar1),
                () -> Assertions.assertSame(car2, fetchedCar2)
        );
    }

    @Test
    void should_return_null_when_fetch_the_car_given_a_wrong_ticket() {
        // Given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);
        ParkingTicket wrongTicket = new ParkingTicket();

        // When
        //Car fetchedCar = parkingLot.fetch(wrongTicket);

        // Then
        //Assertions.assertSame(null,fetchedCar);
        Assertions.assertThrows(UnrecognizedParkingTicketException.class,()-> parkingLot.fetch(wrongTicket));
    }

    @Test
    void should_return_null_when_fetch_the_car_given_a_null_ticket() {
        // Given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        parkingLot.park(car);
        ParkingTicket nullTicket = null;

        // When
//        Car fetchedCar = parkingLot.fetch(nullTicket);

        // Then
//        Assertions.assertSame(null,fetchedCar);
        Assertions.assertThrows(UnrecognizedParkingTicketException.class,()->parkingLot.fetch(nullTicket));
    }

    @Test
    void should_return_null_when_fetch_the_car_given_has_already_been_used_ticket() {
        // Given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);
        Car fetchedCar1 = parkingLot.fetch(parkingTicket);

        // When
//        Car fetchedCar2 = parkingLot.fetch(parkingTicket);

        // Then
        Assertions.assertAll(
                ()->Assertions.assertSame(car,fetchedCar1),
                ()->Assertions.assertThrows(UnrecognizedParkingTicketException.class,()->parkingLot.fetch(parkingTicket))
        );

    }

    @Test
    void should_return_null_when_fetch_the_car_given_a_parking_lot_with_no_position_and_a_car() {
        // Given
        ParkingLot parkingLot = new ParkingLot();
        for (int i = 0; i < 10; i++) {
            Car car = new Car();
            parkingLot.park(car);
        }

        // When
        Car car = new Car();
//        ParkingTicket parkingTicket = parkingLot.park(car);

        // Then
//        Assertions.assertSame(null,parkingTicket);
        Assertions.assertThrows(UnrecognizedParkingTicketException.class,()->parkingLot.park(car));

    }



}
