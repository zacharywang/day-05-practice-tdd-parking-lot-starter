package com.parkinglot;

import java.util.List;

public class ParkingBoy {
    private ParkingLot parkingLot;
    private List<ParkingLot> parkingLots;

    public ParkingBoy(ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }

    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public ParkingTicket park(Car car) {
        if (parkingLots != null && parkingLots.size() != 0) {
            ParkingTicket parkingTicket = null;
            for (ParkingLot singleParkingLot : parkingLots) {
                if (!singleParkingLot.isFull()) {
                    parkingTicket = singleParkingLot.park(car);
                    break;
                }
            }
            if (parkingTicket == null) {
                throw new UnrecognizedParkingTicketException("UnrecognizedParkingTicketException");
            }
            return parkingTicket;
        } else {
            return parkingLot.park(car);
        }
    }

    public Car fetch(ParkingTicket ticket) {
        if (parkingLots != null && parkingLots.size() != 0) {
            Car car = null;
            for (ParkingLot lot : parkingLots) {
                Car fetchedCar = null;
                try {
                    fetchedCar = lot.fetch(ticket);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (fetchedCar != null) {
                    car = fetchedCar;
                }
            }
            if (car == null) {
                throw new UnrecognizedParkingTicketException("Unrecognized parking ticket");
            }
            return car;
        } else {
            return parkingLot.fetch(ticket);
        }
    }
}
